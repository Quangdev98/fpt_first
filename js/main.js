$('#slide-service .owl-carousel').owlCarousel({
	loop: true,
	nav: true,
	dots: false,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 18,
	navText:[`<img src="../images/prev.png" />`, `<img src="../images/next.png" />`],
	responsive: {
		0: {
			items: 1,
		},
		540: {
			items: 2
		}, 768: {
			items: 3
		}
	}
});
$('#banner-main-top').owlCarousel({
	loop: true,
	nav: false,
	dots: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,
	// navText:[`<img src="../images/prev.png" />`, `<img src="../images/next.png" />`],
	responsive: {
		0: {
			items: 1,
		},
	}
});


function resizeImage() {
	let arrClass = [
		{ class: 'resize-new-big', number: (358 / 482) },
		{ class: 'resize-new', number: (134 / 228) },
	];
	for (let i = 0; i < arrClass.length; i++) {
		if($("." + arrClass[i]['class']).length){
			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		}
	}
}

resizeImage();
new ResizeObserver(() => {
	resizeImage();
	
}).observe(document.body)

$('#list-rate').waypoint(function () {
	$(".counter").each(function () {
		var $this = $(this),
			countTo = $this.attr("data-countto");
		countDuration = parseInt($this.attr("data-duration"));
		$({ counter: $this.text() }).animate(
			{
				counter: countTo
			},
			{
				duration: countDuration,
				easing: "linear",
				step: function () {
					$this.text(Math.floor(this.counter));
				},
				complete: function () {
					$this.text(this.counter);
				}
			}
		);
	});
}, {
	offset: '100%'
});


$("#enrollment-method .box-method .title-tabs .item").click(function() {
	let idElement = $(this).attr("attr-link")
	$(this).siblings(".item").removeClass("active")
	$(this).addClass("active")
	$("#enrollment-method .box-method .box-contents .item-content").removeClass("active")
	$(`#enrollment-method .box-method .box-contents .item-content${idElement}`).addClass("active")
})
$('#categoryModal').on('show.bs.modal', function () {
	setTimeout(() => {
		let width = document.getElementsByClassName("resize-image-modal")[0].offsetWidth;
		console.log(width);
		$(".resize-image-modal").css('height', width * (463 / 341) + 'px');
	}, 1000);
})

$(document).ready(function () {

	// menu
	$(".toggle-menu").click(function(){
		$(this).toggleClass("active")
		$("body").toggleClass("active-menu")
		$(".wrap-menu").toggleClass("active")
	})
	$(document).mouseup(function (e) {
		var container = $("header#header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$(".toggle-menu").removeClass("active")
				$("body").removeClass("active-menu")
				$(".wrap-menu").removeClass("active")
		}
	});
	// 
	$('img').each(function () {
		$(this).addClass("lazy");
	});
	  
	// Gọi hàm lazy load
	$(function () {
		$("img.lazy").lazyload();
	});
});